
Piste plan : 

==> Remarques : 

    * Pas beaucoup de chapitres. Une meilleure granularité peut-être? 
        - Ex : Diviser le chapitre 2 en deux chapitres : un chapitre sur les mesures objectives, un autre sur les mesures subjectives. Le chapitre sur les mesures subjectives risque cependant d'être un peu court.

    * L'organisation du chapitre 3 n'est peut-être pas terrible : la division ce qu'on propose / les résultats ne me semble pas si bien fonctionner. 

    * Chapitre 3 : la justification "gain en simplicité" pour justifier l'utilisation d'une caméra monoculaire semble pas très solide. Une meilleure idée de justification?

    * Chapitre 3 : séparation de propositions (section 1 et 2) et résultats (section 3) pas terrible, non?

------------------

Problématique : Comment reproduire l'expertise [manuelle/cognitive/visuelle] d'une personne?

Chapitre 1 : Etat art :

    Section 1 : Comment est définie l'expertise de façon générale? Et comment on cherche à définir cette expertise en robotique?

    Section 2 : Comment qualifie-t-on les différents niveaux de reproduction de l'expertise en robotique? (déport temporel, spatial, partage de contrôle, ...)

    Section 3 : Est-ce vraiment un succès? Réflexion sur le compromis nécessaire en téléopération entre performance et sécurité; conduisant à plus ou moins de "pertes d'informations" et de "perte de dextérité".
        - Ex : "NimbRo Wins ANA Avatar XPRIZE Immersive Telepresence Competition: Human-Centric Evaluation and Lessons Learned" gros système, très coûteux, et performance comparé à être humain? (ex : https://youtu.be/lOnV1Go6Op0?si=P1JiM2cdmNjSAdLJ&t=59516 )

Chapitre 2 : C'est quoi l'important dans ce cas d'application, les critères importants, pour identifier une bonne performance? Comment on propose de mesurer ces critères?

    Section 1 : Via des mesures objectives : 

        SousSection 1 : Confort ergonomique : réflexion sur l'exploitation d'un système de motion capture :
            - Placement optimisé des Optitrack via un casque de réalité augmenté (Hololens 2).
                * On peut imaginer combinaison avec "Optimal Camera Placement for Maximized Motion Capture Volume and Marker Visibility" pour optimiser placement (même si à noter : algo de placement pas ouf d'après souvenirs). 

            - Proposition d'un pipeline d'obtention de posture à partir de données brutes, via Optitrack :
                * Algo labelisation semi-auto, basé sur des recherches de rigid bodies (nécessitant placement particulier de marqueurs sur crps)
                * addbiomechanics local
        
        SousSection 2 : Performance (temps, succès, réalisation des mouvements) : via une comparaison à la réalité : 

            * Valeurs seuils de littérature : 

                - Ex : temps : X2 généralement entre direct et téléopération.

            * Valeurs issues d'enregistrement de techniciens :
                
                - "Types" de mouvements réalisés

                - Temps de réalisation de la tâche

            * Proposition de tâche de simulation

                * NB : l'idée de comparer à la réalité paraît un peu évident, mais de ce que je vois sur litérature téléopération, semble finalement très peu fait? Parfois pour des raisons tout à fait légitime (comme le cadre d'application qui ne permet pas la présence d'humains), mais sinon pratiquement jamais de comparaison entre une réalisation directe de la tâche et une réalisation via robot?

                    * Dans notre cas, semble d'autant plus défendable que les personnes font déjà la tâche d'une certaine façon; proposer une autre façon de le faire n'est intéressant que si on est sûr que si cette nouvelle façon a suffisamment d'avantages par rapport à la méthode employée actuellement. Idée référence pour justifier ça : exemple d'Aérospline avec cobot pour aider à riveter avions.

        SousSection 3 : Sécurité : proposition de critères (sécurité côté robot, visualisation en permanence du end effector, ...)

    Section 2 : via des données subjectives : 

        SousSection 1 : Satisfaction d'utilisation, "expérience utilisateur" : recherche de questionnaires subjectifs adaptés au cadre d'application (et notamment, à la comparaison directe avec la réalité)

        SousSection 2 : Maîtrise des mouvements / agentivité (pour évaluer le degré de perte de dextérité) : SOAS

        SousSection 3 : Qualité visuelle (pour évaluer la perte d'informations pour réaliser la tâche) : présentation de questionnaire "maison", réflexion sur leur réalisation ( échelle de valeur, nombre maximal de questions, formulation des questions, ... selon standard SUS).

Chapitre 3 : C'est quoi les compromis qu'on propose pour garder l'important? Est-ce que ça marche?

    Section 1 : Perte de dextérité au profit de la sécurité : 

        SousSection 1 : Choix de l'outil pour envoyer les commandes (master) : 

            - Interface GUI (cas haut-niveau uniquement : expé 1 en majorité, expé 2 en partie)

            - Handheld device : Quelle technologie utiliser (IMU ou Optitrack)? Comment réaliser l'objet? (Optitrack).

            - Souris3D : Proposition de modélisation des degrés de liberté basés sur les degrés de liberté poignet / main. 

        SousSection 2 : Choix de l'outil pour effectuer les commandes (slave) :

            - Cobot en contrôlant uniquement la trajectoire :

                - Réflexion sur la notion de "trajectoire" : 

                    * Contrôle de trajectoire en temps réel (déport temporel faible)

                    * Contrôle de vitesse/sens de parcours d'un film (déport temporel fort)

                - Pipeline de modulation de trajectoire proposé

                    * Cobot avec tâche de régularisation / contraintes : manipulabilité, espace de travail.

            - 3R (Gimbal, ou Roundabout) en tracking 

                * Réflexion sur le design de la plateforme :

                    - Pas de mouvements de translation possible : avantages procurés (garantie mécanique d'absence de collision notamment).

    Section 2 : Perte d'informations au profit de la simplicité : une unique caméra monoculaire

        SousSection 1 : Un retour visuel toujours possible : définition d'un espace de travail, proposition pour tracking visuel de l'end effector du robot.

        SousSection 2 : Un retour visuel de bonne qualité : proposition via caméra 4K

    Section 3 : Evaluation : est-ce que ça marche?

        SousSection 1 : Expé 1 : Sur l'utilisation d'un cobot en haut niveau

        SousSection 2 : Expé 2 : Sur l'utilisation d'un 3R en tracking
